# Lab 1


def calculator(x, y, operation):
  if x < 0 or x > 10 or y < 0 or y > 10:
    return "Wrong Input number"


# Decision structure listing all possible operations
  if operation == "+":
    return x + y
  elif operation == "-":
    return x - y
  elif operation == "*":
    return x * y
  elif operation == "/":
    return x / y
  else:
    return "Invalid operator"


def main():
  x = int(input("Enter number 0-9: "))
  y = int(input("Enter number 0-10: "))
  operation = input("Enter operator: ")
  answer = calculator(x, y, operation)
  print(answer)


if __name__ == "__main__":
  main()
