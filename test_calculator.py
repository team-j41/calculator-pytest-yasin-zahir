# test_calculator.py

from lab1 import calculator


def test_addition():
  assert calculator(2, 3, "+") == 5


def test_subtraction():
  assert calculator(5, 3, "-") == 2


def test_multiplication():
  assert calculator(2, 3, "*") == 6


def test_division():
  assert calculator(10, 5, "/") == 2


def test_wrong_input():
  assert calculator(-1, 11, "+") == "Wrong Input number"


def test_invalid_operator():
  assert calculator(2, 3, "%") == "Invalid operator"
